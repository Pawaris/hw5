const mongoose = require("mongoose");
const Schema = mongoose.Schema;

//create Student schema & model
const StudentSchema = new Schema({
    name:{
        type: String,
        required: [true,'Name filed is required']
    },
    score:{
        type: Number
    },
    available: {
        type: Boolean,
        default: false
    }

    //add in geo location

});

const Student = mongoose.model('student',StudentSchema);
module.exports =  Student;