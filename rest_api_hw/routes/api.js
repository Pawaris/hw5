const express = require('express');
const router = express.Router();
const Student = require('../models/student')


//get a list of student from the data
router.get('/students', function(req,res,next){
    Student.find({type: Number, score:req.query.score}).then(function(students){
        res.send(students);   
    });
});


//add new student to the data
router.post('/students', function(req,res,next){
    //var student = new student(req.body);
    //student.save();
    Student.create(req.body).then(function(student){
        res.send(student);
    }).catch(next);
});


//update the student in the database
router.put('/students/:id', function(req,res,next){
    console.log("put : "+req.params.id);
    Student.findByIdAndUpdate({_id: req.params.id},req.body).then(function(student){
        Student.findOne({_id: req.params.id}).then(function(student){
            res.send(student);
        });
    });
});


//delete the student from the database
router.delete('/students/:id', function(req,res,next){
    console.log(req.params.id);
    Student.findByIdAndRemove({_id: req.params.id}).then(function(student){
        res.send(student);
    });
});

module.exports = router;